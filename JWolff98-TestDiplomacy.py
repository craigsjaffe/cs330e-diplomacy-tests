#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_write, diplomacy_eval, diplomacy_solve

class TestDiplomacy(TestCase):
    # Unit Tests for diplomacy_read
    def test_diplomacy_read0(self): # Single Army Holds
        r = StringIO("A Austin Hold\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Austin" : ["A"]}, {"A" : [1, ""]}])

    def test_diplomacy_read1(self): # Single Army Moves
        r = StringIO("A Austin Move Madrid\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort() 
        l = [c, a]
        self.assertEqual(l, [{"Austin" : [], "Madrid" : ["A"]}, {"A" : [1, ""]}])

    def test_diplomacy_read2(self): # Two Armies go to battle
        r = StringIO("A Austin Hold\nB Barcelona Move Austin\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Austin" : ["A", "B"], "Barcelona" : []}, {"A" : [1, ""], "B" : [1, ""]}])

    def test_diplomacy_read3(self): # One Army Holds, another Supports
        r = StringIO("A Houston Support B\nB Santiago Hold\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Houston" : ["A"], "Santiago" : ["B"]}, {"A" : [1, "B"], "B" :  [2, ""]}])
    
    def test_diplomacy_read4(self): # Two Armies go to battle, with another supporting the defending army
        r = StringIO("A Austin Support C\nB Barcelona Move Chicago\nC Chicago Hold\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Austin" : ["A"], "Barcelona" : [], "Chicago" : ["B", "C"]}, {"A" : [1, "C"], "B" : [1, ""], "C" : [2, ""]}])
    
    def test_diplomacy_read5(self):# Two Armies go to battle, with another supporting the attacking army
        r = StringIO("A Austin Support B\nB Barcelona Move Chicago\nC Chicago Hold\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Austin" : ["A"], "Barcelona" : [], "Chicago" : ["B", "C"]}, {"A" : [1, "B"], "B" : [2, ""], "C" : [1, ""]}])

    def test_diplomacy_read6(self): # Each Army moves to a different Location.
        r = StringIO("A Austin Move Barcelona\nB Barcelona Move Chicago\nC Chicago Move Dunkirk\nD Dunkirk Move Austin\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Austin" : ["D"], "Barcelona" : ["A"], "Chicago" : ["B"], "Dunkirk" : ["C"]}, {"A" : [1, ""], "B" : [1, ""], "C" : [1, ""], "D" : [1, ""]}])

    def test_diplomacy_read7(self): # Two Armies go to Battle with equal support
        r = StringIO("A Austin Support B\nB Barcelona Move Madrid\nC Chicago Support D\nD Madrid Hold\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Austin" : ["A"], "Barcelona" : [], "Madrid" : ["B", "D"], "Chicago" : ["C"]}, {"A" : [1, "B"], "B" : [2, ""], "C" : [1, "D"], "D" : [2, ""]}])

    def test_diplomacy_read8(self): # Two Armies go to battle in a separate Location while another holds
        r = StringIO("A Austin Move Madrid\nB Barcelona Move Madrid\nC Chicago Hold\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Austin" : [], "Madrid" : ["A", "B"], "Barcelona" : [], "Chicago" : ["C"]}, {"A" : [1, ""], "B" : [1, ""], "C" : [1, ""]}])

    def test_diplomacy_read9(self): # Battle royale with multiple armies with no support
        r = StringIO("A Austin Move Chicago\nB Barcelona Move Chicago\nC Chicago Move Madrid\nD Dunkirk Move Chicago\nE Edmonton Move Chicago\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Austin" : [], "Chicago" : ["A", "B", "D","E"], "Barcelona" : [], "Madrid" : ["C"], "Dunkirk" : [], "Edmonton" : []}, {"A" : [1, ""], "B" : [1, ""], "C" : [1, ""], "D" : [1, ""], "E" : [1, ""]}])

    def test_diplomacy_read10(self): # Battle royale with multiple armies with equal support
        r = StringIO("A Austin Move Chicago\nB Barcelona Support A\nC Chicago Hold\nD Dunkirk Support C\nE Edmonton Move Chicago\nF Fullerton Support E\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Austin" : [], "Chicago" : ["A", "C", "E"], "Barcelona" : ["B"], "Dunkirk" : ["D"], "Edmonton" : [], "Fullerton" : ["F"]}, {"A" : [2, ""], "B" : [1, "A"], "C" : [2, ""], "D" : [1, "C"], "E" : [2, ""], "F" : [1, "E"]}])

    def test_diplomacy_read11(self): # Battle royale with various levels of support
        r = StringIO("A Austin Move Chicago\nB Barcelona Support A\nC Chicago Hold\nD Dunkirk Support A\nE Edmonton Move Chicago\nF Fullerton Support E\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Austin" : [], "Chicago" : ["A", "C", "E"], "Barcelona" : ["B"], "Dunkirk" : ["D"], "Edmonton" : [], "Fullerton" : ["F"]}, {"A" : [3, ""], "B" : [1, "A"], "C" : [1, ""], "D" : [1, "A"], "E" : [2, ""], "F" : [1, "E"]}])

    def test_diplomacy_read12(self): # Supporting city gets attacked
        r = StringIO("A Austin Support C\nB Barcelona Support F\nC Chicago Move Madrid\nD Dunkirk Move Barcelona\nE Edmonton Support B\nF Fullerton Move Madrid\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Austin" : ["A"], "Barcelona" : ["B", "D"], "Chicago" : [], "Madrid" : ["C", "F"], "Dunkirk" : [], "Edmonton" : ["E"], "Fullerton" : []}, {"A" : [1, "C"], "B" : [2, ""], "C" : [2, ""], "D" : [1, ""], "E" : [1, "B"], "F" : [1, ""]}])

    def test_diplomacy_read13(self): # All Armies Support Each other
        r = StringIO("A Austin Support B\nB Barcelona Support A\nC Chicago Support D\nD Dunkirk Support C\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Austin" : ["A"], "Barcelona" : ["B"], "Chicago" : ["C"], "Dunkirk" : ["D"]}, {"A" : [2, "B"], "B" : [2, "A"], "C" : [2, "D"], "D" : [2, "C"]}])

    def test_diplomacy_read14(self): # Armies Support Each other but they get attacked
        r = StringIO("A Austin Support B\nB Barcelona Support A\nC Chicago Support D\nD Dunkirk Support C\nE Edmonton Move Austin\nF FortWorth Move Barcelona\nG Galveston Move Chicago\nH Hancock Move Dunkirk\n")
        c, a = diplomacy_read(r)
        for key in c.keys():
            c[key].sort()
        l = [c, a]
        self.assertEqual(l, [{"Austin" : ["A", "E"], "Barcelona" : ["B", "F"], "Chicago" : ["C", "G"], "Dunkirk" : ["D", "H"], "Edmonton" : [], "FortWorth" : [], "Galveston" : [], "Hancock" : []}, {"A" : [1, ""], "B" : [1, ""], "C" : [1, ""], "D" : [1, ""], "E" : [1, ""], "F" : [1, ""], "G" : [1, ""], "H" : [1, ""]}])

    # Unit Tests for diplomacy_write
    def test_diplomacy_write0(self): # Everyone is alive
        armyLocations = ["A Austin", "B Barcelona", "C Chicago", "D Dunkirk"]
        w = StringIO()
        diplomacy_write(armyLocations, w)
        self.assertEqual(w.getvalue(), "A Austin\nB Barcelona\nC Chicago\nD Dunkirk\n")

    def test_diplomacy_write1(self): # Everyone is dead
        armyLocations = ["A [dead]", "B [dead]", "C [dead]"]
        w = StringIO()
        diplomacy_write(armyLocations, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_diplomacy_write2(self): # Only 1 is dead
        armyLocations = ["A [dead]", "B Barcelona", "C Chicago"]
        w = StringIO()
        diplomacy_write(armyLocations, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Barcelona\nC Chicago\n")

    def test_diplomacy_write3(self): # Only 1 army
        armyLocations = ["A Austin"]
        w = StringIO()
        diplomacy_write(armyLocations, w)
        self.assertEqual(w.getvalue(), "A Austin\n")


    # Unit Tests for diplomacy_eval
    def test_diplomacy_eval0(self): # Only 1 army
        cities = {"Austin" : ["A"]}
        armies = {"A" : [1, ""]}
        armyLocations = diplomacy_eval(cities, armies)
        self.assertEqual(armyLocations, ["A Austin"])
    
    def test_diplomacy_eval1(self): # Two Armies occupying the same city with no support
        cities = {"Austin" : ["A", "B"], "Barcelona" : []}
        armies = {"A" : [1, ""], "B" : [1, ""]}
        armyLocations = diplomacy_eval(cities, armies)
        self.assertEqual(armyLocations, ["A [dead]", "B [dead]"])

    def test_diplomacy_eval2(self): # Two Armies occupying the same city with equal support
        cities = {"Austin" : ["A"], "Barcelona" : ["B", "C"], "Chicago" : [], "Dunkirk" : ["D"]}
        armies = {"A" : [1, "B"], "B" : [2, ""], "C" : [2, ""], "D" : [1, "C"]}
        armyLocations = diplomacy_eval(cities, armies)
        self.assertEqual(armyLocations, ["A Austin", "B [dead]", "C [dead]", "D Dunkirk"])

    def test_diplomacy_eval3(self): # All armies in separate cities with some support
        cities = {"Austin" : ["A"], "Barcelona" : ["B"], "Chicago" : ["C"]}
        armies = {"A" : [2, "B"], "B" : [2, ""], "C" : [1, "A"]}
        armyLocations = diplomacy_eval(cities, armies)
        self.assertEqual(armyLocations, ["A Austin", "B Barcelona", "C Chicago"])

    def test_diplomacy_eval4(self): # Battle royale with equal support
        cities = {"Austin" : [], "Chicago" : ["A", "C", "E"], "Barcelona" : ["B"], "Dunkirk" : ["D"], "Edmonton" : [], "Fullerton" : ["F"]}
        armies = {"A" : [2, ""], "B" : [1, "A"], "C" : [2, ""], "D" : [1, "C"], "E" : [2, ""], "F" : [1, "E"]}
        armyLocations = diplomacy_eval(cities, armies)
        self.assertEqual(armyLocations, ["A [dead]", "B Barcelona", "C [dead]", "D Dunkirk", "E [dead]", "F Fullerton"])

    def test_diplomacy_eval5(self): # Battle royale with various levelsof support
        cities = {"Austin" : [], "Chicago" : ["A", "C", "E"], "Barcelona" : ["B"], "Dunkirk" : ["D"], "Edmonton" : [], "Fullerton" : ["F"]}
        armies = {"A" : [3, ""], "B" : [1, "A"], "C" : [1, ""], "D" : [1, "A"], "E" : [2, ""], "F" : [1, "E"]}
        armyLocations = diplomacy_eval(cities, armies)
        self.assertEqual(armyLocations, ["A Chicago", "B Barcelona", "C [dead]", "D Dunkirk", "E [dead]", "F Fullerton"])


    # Unit Tests for diplomacy_solve
    def test_diplomacy_solve0(self): # Single Army Holds
        r = StringIO("A Austin Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\n")

    def test_diplomacy_solve1(self): # Single Army Moves
        r = StringIO("A Austin Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_diplomacy_solve2(self): # Two Armies go to battle
        r = StringIO("A Austin Hold\nB Barcelona Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_diplomacy_solve3(self): # One Army Holds, another Supports
        r = StringIO("A Houston Support B\nB Santiago Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Houston\nB Santiago\n")
    
    def test_diplomacy_solve4(self): # Two Armies go to battle, with another supporting defending army
        r = StringIO("A Austin Support C\nB Barcelona Move Chicago\nC Chicago Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\nB [dead]\nC Chicago\n")
    
    def test_diplomacy_solve5(self): # Two Armies go to battle, with another supporting the attacking army
        r = StringIO("A Austin Support B\nB Barcelona Move Chicago\nC Chicago Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\nB Chicago\nC [dead]\n")

    def test_diplomacy_solve6(self): # Each Army moves to a different location
        r = StringIO("A Austin Move Barcelona\nB Barcelona Move Chicago\nC Chicago Move Dunkirk\nD Dunkirk Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Chicago\nC Dunkirk\nD Austin\n")
    
    def test_diplomacy_solve7(self): # Two Armies go to Battle with equal support
        r = StringIO("A Austin Support B\nB Barcelona Move Madrid\nC Chicago Support D\nD Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\nB [dead]\nC Chicago\nD [dead]\n")
    
    def test_diplomacy_solve8(self): # Two Armies go to Battle in a separate location while another holds
        r = StringIO("A Austin Move Madrid\nB Barcelona Move Madrid\nC Chicago Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Chicago\n")

    def test_diplomacy_solve9(self): # Battle royale with multiple armies with no support
        r = StringIO("A Austin Move Chicago\nB Barcelona Move Chicago\nC Chicago Move Madrid\nD Dunkirk Move Chicago\nE Edmonton Move Chicago\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Madrid\nD [dead]\nE [dead]\n")

    def test_diplomacy_solve10(self): # Battle royale with multiple armies with equal support
        r = StringIO("A Austin Move Chicago\nB Barcelona Support A\nC Chicago Hold\nD Dunkirk Support C\nE Edmonton Move Chicago\nF Fullerton Support E\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Barcelona\nC [dead]\nD Dunkirk\nE [dead]\nF Fullerton\n")

    def test_diplomacy_solve11(self): # Battle royale with various levels of support
        r = StringIO("A Austin Move Chicago\nB Barcelona Support A\nC Chicago Hold\nD Dunkirk Support A\nE Edmonton Move Chicago\nF Fullerton Support E\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Chicago\nB Barcelona\nC [dead]\nD Dunkirk\nE [dead]\nF Fullerton\n")

    def test_diplomacy_solve12(self): # Supporting city gets attacked
        r = StringIO("A Austin Support C\nB Barcelona Support F\nC Chicago Move Madrid\nD Dunkirk Move Barcelona\nE Edmonton Support B\nF Fullerton Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\nB Barcelona\nC Madrid\nD [dead]\nE Edmonton\nF [dead]\n")

    def test_diplomacy_solve13(self): # Each Army Supports Each other
        r = StringIO("A Austin Support B\nB Barcelona Support A\nC Chicago Support D\nD Dunkirk Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\nB Barcelona\nC Chicago\nD Dunkirk\n")

    def test_diplomacy_solve14(self): # Armies Support each other but each of them get attacked
        r = StringIO("A Austin Support B\nB Barcelona Support A\nC Chicago Support D\nD Dunkirk Support C\nE Edmonton Move Austin\nF FortWorth Move Barcelona\nG Galveston Move Chicago\nH Hancock Move Dunkirk\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\nG [dead]\nH [dead]\n")
if __name__ == "__main__": # pragma: no cover
    main()
