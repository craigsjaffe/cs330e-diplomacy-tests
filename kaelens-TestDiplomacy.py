#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    
    # ----
    # read
    # ----

    def test_read_1(self):
        s = StringIO("A Madrid Hold\n")
        a = diplomacy_read(s)
        self.assertEqual(a,  ["A Madrid Hold"])

    def test_read_2(self):
        s = StringIO("A Madrid Hold\nB Barcelona Hold\n")
        a = diplomacy_read(s)
        self.assertEqual(a,  ["A Madrid Hold", "B Barcelona Hold"])

    def test_read_3(self):
        s = StringIO("A Madrid Support B\nB Barcelona Hold\nC London Move Barcelona")
        a = diplomacy_read(s)
        self.assertEqual(a,  ["A Madrid Support B", "B Barcelona Hold", "C London Move Barcelona"])
        
    # ----
    # eval
    # ----

    def test_eval_1(self):
        a = diplomacy_eval(["A Madrid Hold"])
        self.assertEqual(a,  {"A": "Madrid"})

    def test_eval_2(self):
        a = diplomacy_eval(["A Madrid Move Barcelona", "B Barcelona Move Madrid"])
        self.assertEqual(a,  {"A": "Barcelona", "B": "Madrid"})

    def test_eval_3(self):
        a = diplomacy_eval(["A Madrid Hold", "B Barcelona Hold", "C London Hold"])
        self.assertEqual(a,  {"A": "Madrid", "B": "Barcelona", "C": "London"})
    
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {"A": "[dead]"})
        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {"A": "Madrid", "B": "[dead]"})
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {"A": "Madrid", "B": "Barcelona", "C": "London"})
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\n")
    
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Madrid\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Austin Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC London\nD Austin\n")
        
    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Madrid Move Barcelona\nC Barcelona Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC [dead]\n")
        


# ---
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
...............
----------------------------------------------------------------------
Ran 15 tests in 0.001s

OK

$ coverage report -m                   >> TestDiplomacy.out

$ cat TestDiplomacy.out
...............
----------------------------------------------------------------------
Ran 15 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          64      0     48      0   100%
TestDiplomacy.py      69      0      2      0   100%
--------------------------------------------------------------
TOTAL                133      0     50      0   100%
"""
