#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------


# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = 'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Prague Hold'
        l = diplomacy_read(s)
        self.assertEqual(l,  ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Prague Hold'])
        
    def test_read2(self):
        s = 'A Madrid Hold\nB Austin Move Madrid'
        l = diplomacy_read(s)
        self.assertEqual(l,  ['A Madrid Hold', 'B Austin Move Madrid'])
        
    def test_read3(self):
        s = 'A Madrid Hold\nB London Move Madrid\nC Austin Support B'
        l = diplomacy_read(s)
        self.assertEqual(l,  ['A Madrid Hold', 'B London Move Madrid', 'C Austin Support B'])

    # -----
    # solve
    # -----
    
    def test_solve(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Prague Hold')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD Prague\n')
        
    def test_solve2(self):
        r = StringIO('A Madrid Hold\nB Austin Move Madrid')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\n') 
    
    def test_solve3(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC Austin Support B\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB Madrid\nC Austin\n')
    
    def test_solve4(self):
        r = StringIO('A Madrid Move Barcelona\nB Barcelona Move Madrid')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A Barcelona\nB Madrid\n')
    
    def test_solve5(self):
        r = StringIO('A Austin Support B\nB Boston Support C\nC Charlotte Hold\nD Dallas Move Boston\nE Edmonton Move Charlotte\nF FortWorth Support E\nG Prague Support E')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), 'A Austin\nB Boston\nC [dead]\nD [dead]\nE Charlotte\nF FortWorth\nG Prague\n')
# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()
