from io import StringIO
from unittest import TestCase, main

from Diplomacy import diplomacy_solve


class TestDiplomacy(TestCase):
    def test1(self):
        diplomacy_input = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        output = "A [dead]\nB Madrid\nC [dead]\nD Paris\n"
        writer = StringIO()
        diplomacy_solve(diplomacy_input, writer)
        self.assertEqual(writer.getvalue(), output)

    def test2(self):
        diplomacy_input = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        output = "A [dead]\nB [dead]\nC [dead]\n"
        writer = StringIO()
        diplomacy_solve(diplomacy_input, writer)
        self.assertEqual(writer.getvalue(), output)

    def test3(self):
        diplomacy_input = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        output = "A [dead]\nB [dead]\n"
        writer = StringIO()
        diplomacy_solve(diplomacy_input, writer)
        self.assertEqual(writer.getvalue(), output)

    def test4(self):
        # corner case: army moving to a city that doesn't exist
        diplomacy_input = StringIO("A Madrid Move Houston\nB Austin Move Madrid")
        output = "A Houston\nB Madrid\n"
        writer = StringIO()
        diplomacy_solve(diplomacy_input, writer)
        self.assertEqual(writer.getvalue(), output)

    def test5(self):
        # corner case: army support non-existent army
        diplomacy_input = StringIO("A Madrid Move Houston\nB Austin Support M")
        output = "A Houston\nB Austin\n"
        writer = StringIO()
        diplomacy_solve(diplomacy_input, writer)
        self.assertEqual(writer.getvalue(), output)


if __name__ == "__main__":  # pragma: no cover
    main()
